# technology stack
I used PHP 7.0 on an Ubuntu machine to run the code and pass the tests. The `Orchard.php` is a class with 5 public methods which are my solutions to 5 technical questions. You need to create an object from this class and call them. That's all. 

## Sources
I used stackoverflow and php.net website for technical stuff. Also, I need to mention the HackerRank website which is a great resource to learn algorithm and data structure. Here are the link to this amazing website:
 
 [HackerRank](http://hackerrank.com/)
 
I also tried to leave comments to make the code more readable and easy to follow. Yeah
If you have any question, please let me know. 
Regards 