<?php
/**
 * Created by PhpStorm.
 * User: Vahob
 * Date: 16/07/17
 * Time: 5:16 PM
 */
class Orchard {

    public $answers=array();

    /**
     * @param string $str input string to determine if it's palindrome
     * @return bool|string
     */
    public  function Palindrome($str){
        //trim and turn it into all lower case.
        $str = strtolower(trim($str));
        if($str === '') return 'UNDETERMINED';
        $strArr = str_split($str);
        //remove non-alphanumeric chars
        $strippedArr = array_values(array_filter($strArr,function($value){
            return ctype_alnum($value);
        }));
        $counter = floor(count($strippedArr)/2);
        $length = count($strippedArr)-1;
        foreach ($strippedArr as $key=>$char){
            if($key < $counter && $char !== $strippedArr[$length-$key]){
                return false;
            }
            if($key >= $counter) break;
        }
        return true;
    }
    /**
     * @param string $str
     * @return string
     */
    public function Instgram($str){
        //trim and turn it into all lower case.
        $str = strtolower(trim($str));
        $strArr = str_split($str);
        //remove non-alphanumeric chars and re-index the array
        $strippedArr = array_values(array_filter($strArr,function($value){
            return ctype_alnum($value);
        }));
        $type = "NOTAGRAM";
        //count occurrence the first letter to have a base and see it has more than 1 occurrence
        $firstChar = $strippedArr[0];
        $baseCounter = count(array_filter($strippedArr,function($char) use ($firstChar) {
            return $char === $firstChar;
        }));
        //add o counted characters to decrease the computation
        $counted = array($str[0]);
        foreach ($strippedArr as $key => $char){
            //already  counted. skip to next character
            if(in_array($char,$counted)) continue;
            //find all the occurrence of the current character
            $counter = count(array_filter($strippedArr,function($ch) use ($char) {
                return $ch === $char;
            }));
            //if this is not equal then, neithe iso nor hetro.
            if($counter !== $baseCounter){
                //turn base counter to -1 for evaluation at the end.
                $baseCounter = -1;
                break;
            }
            $counted[] = $char;
        }
        if ($baseCounter === 1) {// all chars happen once
            $type = 'HETEROGRAM';
        }elseif ($baseCounter > 1) {
            $type = 'ISOGRAM';
        }
        return $type;
    }
    /**
     * @param string $time a string of  time hh:mm:ss
     * @return string the angle between hour and minute hand + degrees as a string
     */
    public function AngularTM($time){
        $timeArr = explode(":",$time);
        $minutes = intval( $timeArr[1] );
        $hours = intval($timeArr[0]) === 12 ? 0 :intval($timeArr[0]);
        $angle = abs(60*$hours - 11*$minutes)/2;
        return $angle . ' degrees';
    }
    public function Magic8Ball($question){
        $question = strtolower(trim($question));
        //check if it is a question at all
        if(strpos($question,'?') === false){
            return 'Not a question';
        }
        $questioArr = explode('?',$question);
        $realQuestionArr = str_split(trim($questioArr[0]));
        $realQuestion = implode("",array_filter($realQuestionArr,function ($value){
            return ctype_alnum($value);
        }));
        $answers = array(
            "It is certain",
            "It is decidedly so",
            "Without a doubt",
            "Yes – definitely",
            "You may rely on it",
            "As I see it",
            "Yes",
            "Most Likely",
            "Outlook good",
             "Signs point to yes"
        );
        if(array_key_exists($realQuestion,$this->answers)){
            $answer = $this->answers["{$realQuestion}"];
        }else{
            $answer = $answers[array_rand($answers)];
            $this->answers[$realQuestion] = $answer;
        }
        return  $answer;
    }
    /**
     * @return  void print a string or number based on conditions
     */
    public function SnapCracklePop(){
        $counter = 1;
        while ($counter < 100){
            if($counter % 3 === 0 && $counter % 15 !==0){
                $output = "SNAP";
            }elseif ($counter % 5 === 0 && $counter % 15 !==0) {
                $output = "CRACKLE";
            }elseif($counter % 15 ===0){
                $output = "POP";
            }else{
                $output = $counter;
            }
            print $output.PHP_EOL;
            $counter++;
        }
    }
}
$orchard = new Orchard();

var_dump($orchard->Palindrome('emordnilaP'));
var_dump($orchard->Palindrome(''));
var_dump($orchard->Palindrome('Kayak'));
var_dump($orchard->Palindrome('No lemon, no melon'));

var_dump($orchard->Instgram('uncopyrightable'));
var_dump($orchard->Instgram('Caucasus'));
var_dump($orchard->Instgram('authorising'));

var_dump($orchard->AngularTM('12:00:00'));
var_dump($orchard->AngularTM('12:15:00'));
var_dump($orchard->AngularTM('11:40:00'));
var_dump($orchard->Magic8Ball('Will i pass this test? '));
var_dump($orchard->Magic8Ball('Will I fail this test?'));
var_dump($orchard->Magic8Ball('WILL I PASS THIS TEST?'));
var_dump($orchard->Magic8Ball('Will i pass this test? Kind Regards'));
var_dump($orchard->Magic8Ball('Will i pass this test'));

$orchard->SnapCracklePop();
